# download and run docker-compose image
docker run docker/compose:1.22.0 version

# create ~/.bashrc if not exists
touch ~/.bash_profile

# create alias "docker-compose"
echo alias docker-compose="'"'docker run --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v "$PWD:$PWD" \
    -w="$PWD" \
    docker/compose:1.22.0'"'" >> ~/.bash_profile

echo alias dc=docker-compose >> ~/.bash_profile

# reload bash config
source ~/.bash_profile

# clone projects
git clone https://gitlab.com/olinglEt/jwt-auth.git
git clone https://gitlab.com/olinglEt/forum-server.git
git clone https://gitlab.com/olinglEt/forum-client.git

touch ./forum-server/server.env
echo "JWT_AUTH_SERVER_HOST=http://<your ip address>:5000" >> ./forum-server/server.env

touch ./forum-client/.env
echo -e "VUE_APP_ENDPOINT_AUTH=http://<your ip address>:5000\n
VUE_APP_ENDPOINT_API=http://<your ip address>:5001" >> ./forum-client/.env